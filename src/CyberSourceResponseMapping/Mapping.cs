﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberSourceResponseMapping
{
    public class Mapping
    {

            private List<CyberSourceTransactionResponse> InstantiateResponseCodes()
        { 
            string code0 = "Success";
            string code1 = "Your bank has reported the address information does not match your billing address.";
            string code2 = "Your bank has reported the security code entered does not match your credit card.";
            string code3 = "Your bank has reported the credit card number or expiration date you entered is invalid.";
            string code4 = "Transaction held for Review, please contact your bank.";
            string code5 = "A transaction with identical amount and credit card information was submitted two minutes prior.";
            string code6 = "Your bank has declined this transaction.";
            string code7 = "There was an error processing this transaction, please try again or contact your bank.";
            string code8 = "Non-U.S. Card Issuing Banks are not accepted.";
            string error = "We are sorry but our payment system is experiencing problems, please try again later.";

            List<CyberSourceTransactionResponse> _return = new List<CyberSourceTransactionResponse>
                {
                    new CyberSourceTransactionResponse() { Code=100, FriendlyDescription=code0, FlagString="SOK", Description="Successful transaction."},
                    new CyberSourceTransactionResponse() { Code=101, FriendlyDescription=code7, FlagString="DMISSINGFIELD", Description="Declined - The request is missing one or more fields."},
                    new CyberSourceTransactionResponse() { Code=102, FriendlyDescription=code7, FlagString="DINVALIDDATA", Description="Declined - One or more fields in the request contains invalid data."},
                    new CyberSourceTransactionResponse() { Code=104, FriendlyDescription=code5, FlagString="DDUPLICATE", Description="Declined - ThemerchantReferenceCode sent with this authorization request matches the merchantReferenceCode of another authorization request that you sent in the last 15 minutes."},
                    new CyberSourceTransactionResponse() { Code=110, FriendlyDescription=code0, FlagString="SPARTIALAPPROVAL", Description="Partial amount was approved."},
                    new CyberSourceTransactionResponse() { Code=150, FriendlyDescription=error, FlagString="ESYSTEM", Description="Error - General system failure."},
                    new CyberSourceTransactionResponse() { Code=151, FriendlyDescription=error, FlagString="ETIMEOUT", Description="Error - The request was received but there was a server timeout. This error does not include timeouts between the client and the server."},
                    new CyberSourceTransactionResponse() { Code=152, FriendlyDescription=error, FlagString="ETIMEOUT", Description="Error: The request was received, but a service did not finish running in time."},
                    new CyberSourceTransactionResponse() { Code=200, FriendlyDescription=code1, FlagString="DAVSNO", Description="Soft Decline - The authorization request was approved by the issuing bank but declined by CyberSource because it did not pass the Address Verification Service (AVS) check."},
                    new CyberSourceTransactionResponse() { Code=201, FriendlyDescription=code6, FlagString="DCALL", Description="Decline - The issuing bank has questions about the request. You do not receive an authorization code programmatically, but you might receive one verbally by calling the processor."},
                    new CyberSourceTransactionResponse() { Code=202, FriendlyDescription=code3, FlagString="DCARDEXPIRED", Description="Decline - Expired card. You might also receive this if the expiration date you provided does not match the date the issuing bank has on file.  Note: The ccCreditService does not check the expiration date; instead, it passes the request to the payment processor. If the payment processor allows issuance of credits to expired cards, CyberSource does not limit this functionality."},
                    new CyberSourceTransactionResponse() { Code=203, FriendlyDescription=code2, FlagString="DCARDREFUSED", Description="Decline - General decline of the card. No other information provided by the issuing bank."},
                    new CyberSourceTransactionResponse() { Code=204, FriendlyDescription=code6, FlagString="DCARDREFUSED", Description="Decline - Insufficient funds in the account."},
                    new CyberSourceTransactionResponse() { Code=205, FriendlyDescription=code6, FlagString="DCARDREFUSED", Description="Decline - Stolen or lost card."},
                    new CyberSourceTransactionResponse() { Code=207, FriendlyDescription=code7, FlagString="DCARDREFUSED", Description="Decline - Issuing bank unavailable."},
                    new CyberSourceTransactionResponse() { Code=208, FriendlyDescription=code6, FlagString="DCARDREFUSED", Description="Decline - Inactive card or card not authorized for card-not-present transactions."},
                    new CyberSourceTransactionResponse() { Code=209, FriendlyDescription=code2, FlagString="DCARDREFUSED", Description="Decline - American Express Card Identification Digits (CID) did not match."},
                    new CyberSourceTransactionResponse() { Code=210, FriendlyDescription=code6, FlagString="DCARDREFUSED", Description="Decline - The card has reached the credit limit."},
                    new CyberSourceTransactionResponse() { Code=211, FriendlyDescription=code2, FlagString="DCARDREFUSED", Description="Decline - Invalid Card Verification Number (CVN)."},
                    new CyberSourceTransactionResponse() { Code=220, FriendlyDescription=code6, FlagString="DCHECKREFUSED", Description="Decline - Generic Decline."},
                    new CyberSourceTransactionResponse() { Code=221, FriendlyDescription=error, FlagString="DCHECKREFUSED", Description="Decline - The customer matched an entry on the processor's negative file."},
                    new CyberSourceTransactionResponse() { Code=222, FriendlyDescription=code6, FlagString="DCHECKREFUSED", Description="Decline - customer's account is frozen."},
                    new CyberSourceTransactionResponse() { Code=230, FriendlyDescription=code2, FlagString="DCV", Description="Soft Decline - The authorization request was approved by the issuing bank but declined by CyberSource because it did not pass the card verification number (CVN) check."},
                    new CyberSourceTransactionResponse() { Code=231, FriendlyDescription=code3, FlagString="DINVALIDCARD", Description="Decline - Invalid account number."},
                    new CyberSourceTransactionResponse() { Code=232, FriendlyDescription=error, FlagString="DINVALIDDATA", Description="Decline - The card type is not accepted by the payment processor."},
                    new CyberSourceTransactionResponse() { Code=233, FriendlyDescription=code6, FlagString="DINVALIDDATA", Description="Decline - General decline by the processor."},
                    new CyberSourceTransactionResponse() { Code=234, FriendlyDescription=error, FlagString="DINVALIDDATA", Description="Decline - There is a problem with your CyberSource merchant configuration."},
                    new CyberSourceTransactionResponse() { Code=235, FriendlyDescription=error, FlagString="DINVALIDDATA", Description="Decline - The requested amount exceeds the originally authorized amount. Occurs, for example, if you try to capture an amount larger than the original authorization amount."},
                    new CyberSourceTransactionResponse() { Code=236, FriendlyDescription=error, FlagString="DINVALIDDATA", Description="Decline - Processor failure."},
                    new CyberSourceTransactionResponse() { Code=237, FriendlyDescription=error, FlagString="DINVALIDDATA", Description="Decline - The authorization has already been reversed."},
                    new CyberSourceTransactionResponse() { Code=238, FriendlyDescription=error, FlagString="DINVALIDDATA", Description="Decline - The transaction has already been settled."},
                    new CyberSourceTransactionResponse() { Code=239, FriendlyDescription=error, FlagString="DINVALIDDATA", Description="Decline - The requested transaction amount must match the previous transaction amount."},
                    new CyberSourceTransactionResponse() { Code=240, FriendlyDescription=error, FlagString="DINVALIDDATA", Description="Decline - The card type sent is invalid or does not correlate with the credit card number."},
                    new CyberSourceTransactionResponse() { Code=241, FriendlyDescription=error, FlagString="DINVALIDDATA", Description="Decline - The referenced request id is invalid for all follow-on transactions."},
                    new CyberSourceTransactionResponse() { Code=242, FriendlyDescription=error, FlagString="DNOAUTH", Description="Decline - The request ID is invalid.  You requested a capture, but there is no corresponding, unused authorization record. Occurs if there was not a previously successful authorization request or if the previously successful authorization has already been used in another capture request."},
                    new CyberSourceTransactionResponse() { Code=243, FriendlyDescription=error, FlagString="DINVALIDDATA", Description="Decline - The transaction has already been settled or reversed."},
                    new CyberSourceTransactionResponse() { Code=246, FriendlyDescription=error, FlagString="DNOTVOIDABLE", Description="Decline - The capture or credit is not voidable because the capture or credit information has already been submitted to your processor. Or, you requested a void for a type of transaction that cannot be voided."},
                    new CyberSourceTransactionResponse() { Code=247, FriendlyDescription=error, FlagString="DINVALIDDATA", Description="Decline - You requested a credit for a capture that was previously voided."},
                    new CyberSourceTransactionResponse() { Code=248, FriendlyDescription=error, FlagString="DBOLETODECLINED", Description="Decline - The boleto request was declined by your processor."},
                    new CyberSourceTransactionResponse() { Code=250, FriendlyDescription=error, FlagString="ETIMEOUT", Description="Error - The request was received, but there was a timeout at the payment processor."},
                    new CyberSourceTransactionResponse() { Code=251, FriendlyDescription=error, FlagString="DCARDREFUSED", Description="Decline - The Pinless Debit card's use frequency or maximum amount per use has been exceeded."},
                    new CyberSourceTransactionResponse() { Code=254, FriendlyDescription=error, FlagString="DINVALIDDATA", Description="Decline - Account is prohibited from processing stand-alone refunds."},
                    new CyberSourceTransactionResponse() { Code=400, FriendlyDescription=code4, FlagString="DSCORE", Description="Soft Decline - Fraud score exceeds threshold."},
                    new CyberSourceTransactionResponse() { Code=450, FriendlyDescription=code1, FlagString="DINVALIDADDRESS", Description="Apartment number missing or not found."},
                    new CyberSourceTransactionResponse() { Code=451, FriendlyDescription=code1, FlagString="DINVALIDADDRESS", Description="Insufficient address information."},
                    new CyberSourceTransactionResponse() { Code=452, FriendlyDescription=code1, FlagString="DINVALIDADDRESS", Description="House/Box number not found on street."},
                    new CyberSourceTransactionResponse() { Code=453, FriendlyDescription=code1, FlagString="DINVALIDADDRESS", Description="Multiple address matches were found."},
                    new CyberSourceTransactionResponse() { Code=454, FriendlyDescription=code1, FlagString="DINVALIDADDRESS", Description="P.O. Box identifier not found or out of range."},
                    new CyberSourceTransactionResponse() { Code=455, FriendlyDescription=code1, FlagString="DINVALIDADDRESS", Description="Route service identifier not found or out of range."},
                    new CyberSourceTransactionResponse() { Code=456, FriendlyDescription=code1, FlagString="DINVALIDADDRESS", Description="Street name not found in Postal code."},
                    new CyberSourceTransactionResponse() { Code=457, FriendlyDescription=code1, FlagString="DINVALIDADDRESS", Description="Postal code not found in database."},
                    new CyberSourceTransactionResponse() { Code=458, FriendlyDescription=code1, FlagString="addressIDADDRESS", Description="Unable to verify or correct address."},
                    new CyberSourceTransactionResponse() { Code=459, FriendlyDescription=code1, FlagString="DINVALIDADDRESS", Description="Multiple addres matches were found (international)."},
                    new CyberSourceTransactionResponse() { Code=460, FriendlyDescription=code1, FlagString="DINVALIDADDRESS", Description="Address match not found (no reason given)."},
                    new CyberSourceTransactionResponse() { Code=461, FriendlyDescription=code1, FlagString="DINVALIDADDRESS", Description="Unsupported character set."},
                    new CyberSourceTransactionResponse() { Code=475, FriendlyDescription=error, FlagString="DAUTHENTICATE", Description="The cardholder is enrolled in Payer Authentication. Please authenticate the cardholder before continuing with the transaction."},
                    new CyberSourceTransactionResponse() { Code=476, FriendlyDescription=error, FlagString="DAUTHENTICATIONFAILED", Description="Encountered a Payer Authentication problem. Payer could not be authenticated."},
                    new CyberSourceTransactionResponse() { Code=480, FriendlyDescription=code0, FlagString="DREVIEW", Description="The order is marked for review by Decision Manager."},
                    new CyberSourceTransactionResponse() { Code=481, FriendlyDescription=code1, FlagString="DREJECT", Description="The order has been rejected by Decision Manager."},
                    new CyberSourceTransactionResponse() { Code=520, FriendlyDescription=code6, FlagString="DSETTINGS", Description="Soft Decline - The authorization request was approved by the issuing bank but declined by CyberSource based on your Smart Authorization settings."},
                    new CyberSourceTransactionResponse() { Code=700, FriendlyDescription=code6, FlagString="DRESTRICTED", Description="The customer matched the Denied Parties List."},
                    new CyberSourceTransactionResponse() { Code=701, FriendlyDescription=code8, FlagString="DRESTRICTED", Description="Export bill_country/ship_country match."},
                    new CyberSourceTransactionResponse() { Code=702, FriendlyDescription=code8, FlagString="DRESTRICTED", Description="Export email_country match."},
                    new CyberSourceTransactionResponse() { Code=703, FriendlyDescription=code8, FlagString="DRESTRICTED", Description="Export hostname_country/ip_country match."}
                };
            return _return;
        }
    }
    
}
