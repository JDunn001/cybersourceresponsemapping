﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberSourceResponseMapping
{
    public class CyberSourceTransactionResponse
    {
        public int Code { get; set; }
        public string FlagString { get; set; }
        public string Description { get; set; }
        public string FriendlyDescription { get; set; }
    }
}
